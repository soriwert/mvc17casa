<!DOCTYPE html>
<html>
<head>
    <title>Edición de usuario</title>
</head>
<body>
    <header>Cabecera <hr></header>
    <content>
        <h1>Edición de usuario</h1>

        <form action="/mvc17/v4/user/update/<?php echo $data['user']->id?>" method="post">
            <label>Nombre</label>
            <input type="text" name="name" value="<?php echo $data['user']->name?>">
            <br>

            <label>Apellidos</label>
            <input type="text" name="surname" value="<?php echo $data['user']->surname?>">
            <br>

            <label>Edad</label>
            <input type="text" name="age" value="<?php echo $data['user']->age?>">
            <br>

            <label>Email</label>
            <input type="text" name="email" value="<?php echo $data['user']->email?>">
            <br>

            <input type="submit" value="Guardar">


        </form>

    </content>
    <footer> <hr> Pie</footer>
</body>
</html>
<!-- https://bitbucket.org/daw2rafa/mvc17 -->
