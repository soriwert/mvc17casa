<!DOCTYPE html>
<html>
<head>
    <title>Detalle de usuario</title>
</head>
<body>
    <header>Cabecera <hr></header>
    <content>
        <h1>Detalle de usuario</h1>
        <ul>

            <li>Nombre:
                <?php echo $data['user']->name ?>
            </li>
            <li>Apellidos:
                <?php echo $data['user']->surname ?>
            </li>
            <li>Edad:
                <?php echo $data['user']->age ?>
            </li>
            <li>E-mail:
                <?php echo $data['user']->email ?>
            </li>
        </ul>
    </content>
    <footer> <hr> Pie</footer>
</body>
</html>
