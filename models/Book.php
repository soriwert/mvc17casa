<?php
require_once 'Cathegory.php';
/**
*
*/
class Book
{
    public $id;
    public $title;
    public $author;
    public $pages;
    public $cathegory_id;

    function __construct()
    {

    }

    public function connect()
    {
        $dsn = 'mysql:dbname=mvc17;host=127.0.0.1';
        $usuario = 'root';
        $contraseña = 'root';

        try {
            $db = new PDO($dsn, $usuario, $contraseña, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES  \'UTF8\''));
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // die('Conectado');
        } catch (PDOException $e) {
            die ('Falló la conexión: ' . $e->getMessage());
        }
        return $db;
    }

    public static function all()
    {
        $db = Book::connect();

        $stmt = $db->query('SELECT * FROM books');
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'Book');
        $results = $stmt->fetchAll();
        return $results;
    }

    public static function find($id)
    {
        $db = User::connect();

        $stmt = $db->query('SELECT * FROM books WHERE id=' . $id);
        $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');
        $result = $stmt->fetch();
        return $result;
    }

    public function save()
    {
        $db = $this->connect();
        $sql = "update books set title=:ntitle where id=:id";
        $query = $db->prepare($sql);
        $query->bindParam(":name", $this->name);
        $query->bindParam(":id", $this->id);
        $result = $query->execute();
        // return $result;
    }


    public function store()
    {
        $db = $this->connect();

        $sql = "INSERT INTO books(title, author, pages, cathegory_id) VALUES(?, ?, ?, ?)";

        $query = $db->prepare($sql);
        $query->bindParam(1, $this->title);
        $query->bindParam(2, $this->author);
        $query->bindParam(3, $this->pages);
        $query->bindParam(4, $this->cathegory_id);

        return $query->execute();
    }

    public function cathegory()
    {
        //Si no existe el atributo cathegory buscar en BBDD
        if (!isset($this->cathegory)) {
            $this->cathegory = Cathegory::find($this->cathegory_id);
        }
            //pegar el atributo al modelo

        //devolver el atributo del modelo book ($this-> ...)
        return $this->cathegory;
    }
    public function __get($name)
    {
        return $this->$name();
    }
}
